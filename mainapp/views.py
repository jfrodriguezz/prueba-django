from django.shortcuts import render, HttpResponse
from .models import *
from django.contrib import messages
from django.shortcuts import redirect

# Create your views here.

def index(request):
    articulos = Articulo.objects.all()
    context = {
        'articulos' : articulos
    }
    return render(request, 'index.html', context)

def guardar_articulo(request):
    if request.method=="POST":
        nombre = request.POST['nombre']
        descripcion = request.POST['descripcion']
        articulo = Articulo(nombre = nombre, descripcion = descripcion)
        articulo.save()
        messages.info(request, "ARTICULO GUARDADO")
    else:
        pass

    articulos = Articulo.objects.all()
    context = {
        'articulos' : articulos
    }
    
    # return render(request, 'index.html', context)
    return redirect (index)


def editar_articulo(request, myid):
    articulo = Articulo.objects.get(id = myid)
    articulos = Articulo.objects.all()
    context = {
        'articulo' : articulo,
        'articulos' : articulos
    }
    return render(request, 'index.html', context)

def actualizar_articulo(request, myid):
    articulo = Articulo.objects.get(id = myid)
    articulo.nombre = request.POST['nombre']
    articulo.descripcion = request.POST['descripcion']
    articulo.save()
    messages.info(request, "ARTICULO ACTUALIZADO")
    return redirect (index)


def eliminar_articulo(request, myid):
    articulo = Articulo.objects.get(id = myid)
    articulo.delete()
    messages.info(request, "ARTICULO ELIMINADO")
    return redirect (index)

