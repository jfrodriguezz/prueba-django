from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('guardar_articulo', guardar_articulo, name='guardar_articulo'),
    path('editar_articulo/<int:myid>/', editar_articulo, name='editar_articulo'),
    path('actualizar_articulo/<int:myid>/', actualizar_articulo, name='actualizar_articulo'),
    path('eliminar_articulo/<int:myid>/', eliminar_articulo, name='eliminar_articulo'),
]